import sys, os, glob
import subprocess
import numpy as np

def gather_results(path,ext):
    dictionary = {}
    dictionary['flow'] = {}
    dictionary['tran'] = {}
    dictionary['total_run_time'] = 0.0
    for root, dirs, files in os.walk(path+"regression_tests"):
        for file in files:
            flow_flag = False
            tran_flag = False
            if file.endswith(ext):
                # print(os.path.join(root, file))
                with open(os.path.join(root, file)) as search:
                    filename = file.split('.')[0]
                    dictionary['flow'][filename] = {}
                    dictionary['tran'][filename] = {}
                    for line in search:
                        line_list = line.strip().split(":")
                        if line_list[0] == '-- SOLUTION':
                            if line_list[1].strip() == 'Flow --':
                                flow_flag = True
                                tran_flag = False
                            elif line_list[1].strip() == 'Transport --':
                                flow_flag = False
                                tran_flag = True
                        if line_list[0] == 'Time Steps':
                            if flow_flag:
                                dictionary['flow'][filename]['ts'] = \
                                int(line_list[1].strip())
                            if tran_flag:
                                dictionary['tran'][filename]['ts'] = \
                                int(line_list[1].strip())
                        elif line_list[0] == 'Newton Iterations':
                            if flow_flag:
                                dictionary['flow'][filename]['ni'] = \
                                int(line_list[1].strip())
                            if tran_flag:
                                dictionary['tran'][filename]['ni'] = \
                                int(line_list[1].strip())
                        elif line_list[0] == 'Solver Iterations':
                            if flow_flag:
                                dictionary['flow'][filename]['si'] = \
                                int(line_list[1].strip())
                            if tran_flag:
                                dictionary['tran'][filename]['si'] = \
                                int(line_list[1].strip())
                        elif line_list[0] == 'Solution 2-Norm':
                            if flow_flag:
                                dictionary['flow'][filename]['sn'] = \
                                float(line_list[1].strip())
                            if tran_flag:
                                dictionary['tran'][filename]['sn'] = \
                                float(line_list[1].strip())
                        elif line_list[0] == 'Residual 2-Norm':
                            if flow_flag:
                                dictionary['flow'][filename]['rn'] = \
                                float(line_list[1].strip())
                            if tran_flag:
                                dictionary['tran'][filename]['rn'] = \
                                float(line_list[1].strip())
    testlog_list = []
    for file in os.listdir(pflotran_path+"regression_tests"):
        if file.endswith(".testlog"):
            testlog_list.append(file)
    testlog_list.sort()

    with open(path+"regression_tests/"+testlog_list[-1]) as search:
        for line in search:
            line_list = line.strip().split(":")
            if line_list[0] == 'Total run time':
                dictionary['total_run_time'] = (line_list[1].strip())
    return dictionary

def modify_tolerance(path,flow_rtol,tran_rtol,flow_atol,tran_atol):
    with open(path+'regression_tests/regression_tests.py','r+') as f:
        new_f = f.readlines()
        f.seek(0)
        for line in new_f:
            if '#heeho: py script replaces this line' not in line:
                f.write(line)
            if '#heeho: test linear tolerance' in line:
                f.write('\n' + '        command.append("-flow_ksp_rtol") ' +
                             '#heeho: py script replaces this line')
                f.write('\n' + '        command.append("' + str(flow_rtol) + '") ' +
                             '         #heeho: py script replaces this line')
                f.write('\n' + '        command.append("-tran_ksp_rtol") ' +
                             '#heeho: py script replaces this line')
                f.write('\n' + '        command.append("' + str(tran_rtol) + '") ' +
                             '         #heeho: py script replaces this line')
                f.write('\n' + '        command.append("-flow_ksp_atol") ' +
                             '#heeho: py script replaces this line')
                f.write('\n' + '        command.append("' + str(flow_atol) + '") ' +
                             '         #heeho: py script replaces this line')
                f.write('\n' + '        command.append("-tran_ksp_atol") ' +
                             '#heeho: py script replaces this line')
                f.write('\n' + '        command.append("' + str(tran_atol) + '") ' +
                             '         #heeho: py script replaces this line')
        f.truncate()

def dict_compare(d1, d2):
    d1_keys = set(d1.keys())
    d2_keys = set(d2.keys())
    intersect_keys = d1_keys.intersection(d2_keys)
    added = d1_keys - d2_keys
    removed = d2_keys - d1_keys
    modified = {o : (d1[o], d2[o]) for o in intersect_keys if d1[o] != d2[o]}
    same = set(o for o in intersect_keys if d1[o] == d2[o])
    return added, removed, modified, same

                
pflotran_path = '/home/heeho/src/pflotran-main/'
pflotran_src = pflotran_path + 'src/pflotran/'
pflotran_exe = pflotran_src + 'pflotran'

run_pflotran_with_new_tolerance = True
flow_rtol = 1e-5
tran_rtol = 1e-5
flow_atol = 1e-50
tran_atol = 1e-50

if run_pflotran_with_new_tolerance:
    modify_tolerance(pflotran_path,flow_rtol,tran_rtol,flow_atol,tran_atol)
    os.system('cd ' + pflotran_src + '; make rtest')

gold = gather_results(pflotran_path,".gold")
reg = gather_results(pflotran_path,".regression")          

f_added, f_removed, f_modified, f_same = dict_compare(gold['flow'], reg['flow'])
t_added, t_removed, t_modified, t_same = dict_compare(gold['tran'], reg['tran'])

gold_flow = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}
reg_flow = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}
gold_tran = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}
reg_tran = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}
gold_flow_same = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}
reg_flow_same = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}
gold_tran_same = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}
reg_tran_same = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}
gold_flow_np = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}
reg_flow_np = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}
gold_tran_np = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}
reg_tran_np = {'ni':[],'si':[],'rn':[],'sn':[],'ts':[]}

for filename in f_modified:
    for key in gold_flow.keys():
        if key in gold['flow'][filename].keys():
            gold_flow[key].append(gold['flow'][filename][key])
            reg_flow[key].append(reg['flow'][filename][key])
            if '-np' in filename:
                gold_flow_np[key].append(gold['flow'][filename][key])
                reg_flow_np[key].append(reg['flow'][filename][key])

for filename in t_modified:
    for key in gold_tran.keys():
        if key in gold['tran'][filename].keys():
            gold_tran[key].append(gold['tran'][filename][key])
            reg_tran[key].append(reg['tran'][filename][key])
            if '-np' in filename:
                gold_tran_np[key].append(gold['tran'][filename][key])
                reg_tran_np[key].append(reg['tran'][filename][key])

for filename in f_same:
    for key in gold_flow.keys():
        if key in gold['flow'][filename].keys():
            gold_flow_same[key].append(gold['flow'][filename][key])
            reg_flow_same[key].append(reg['flow'][filename][key])

for filename in t_same:
    for key in gold_tran.keys():
        if key in gold['tran'][filename].keys():
            gold_tran_same[key].append(gold['tran'][filename][key])
            reg_tran_same[key].append(reg['tran'][filename][key])

           
for key in gold_tran.keys():
    keyname = key + 'avg'
    keyname2 = key + 'avgall'
    keyname3 = key + 'np'
    gold_tran[keyname3] = np.mean(gold_tran_np[key])
    reg_tran[keyname3] = np.mean(reg_tran_np[key])
    gold_flow[keyname3] = np.mean(gold_flow_np[key])
    reg_flow[keyname3] = np.mean(reg_flow_np[key])
    gold_tran[keyname] = np.mean(gold_tran[key])
    reg_tran[keyname] = np.mean(reg_tran[key])
    gold_flow[keyname] = np.mean(gold_flow[key])
    reg_flow[keyname] = np.mean(reg_flow[key])
    gold_tran[keyname2] = np.mean(gold_tran[key]+gold_tran_same[key])
    reg_tran[keyname2] = np.mean(reg_tran[key]+reg_tran_same[key])
    gold_flow[keyname2] = np.mean(gold_flow[key]+gold_flow_same[key])
    reg_flow[keyname2] = np.mean(reg_flow[key]+reg_flow_same[key])
    
    
with open('RegrTestnewtol_%1.e_%1.e_%1.e_%1.e.stdout'
          % (flow_atol, flow_rtol, tran_atol, tran_rtol), 'w+') as f:
    print >> f, "=== Delta Tolerance Regression Test Results ==="
    print >> f, "  flow atol: %1.2e    flow rtol: %1.2e" % (flow_atol, flow_rtol)
    print >> f, "  tran atol: %1.2e    tran rtol: %1.2e" % (tran_atol, tran_rtol)
    print >> f, ""
    print >> f, "  - gold file uses rtol: 1e-5 and atol: 1e-50"
    print >> f, "  - The results only reflect test cases that"
    print >> f, "    were affected by the tolerance"
    print >> f, ""
    print >> f, "  FLOW: number of different results: %i" % len(reg_flow['ts'])
    print >> f, "  FLOW:            GOLD          NEW RTOL: %1.e" % flow_rtol
    print >> f, "  Timestep       %.5g  \t     %.5g" % ((gold_flow['tsavg']),
                                                        (reg_flow['tsavg']))
    print >> f, "  Newton         %.5g  \t     %.5g" % ((gold_flow['niavg']),
                                                        (reg_flow['niavg']))
    print >> f, "  Newton/TS      %.5g  \t     %.5g" % (gold_flow['niavg']/gold_flow['tsavg'],
                                                        reg_flow['niavg']/reg_flow['tsavg'])
    print >> f, "  Linear         %.5g  \t     %.5g" % ((gold_flow['siavg']),
                                                        (reg_flow['siavg']))
    print >> f, "  Linear/Newt    %.5g  \t     %.5g" % (gold_flow['siavg']/gold_flow['niavg'],
                                                        reg_flow['siavg']/reg_flow['niavg'])
    print >> f, "  Sol2Norm       %1.4e \t  %1.4e" % ((gold_flow['snavg']),
                                                      (reg_flow['snavg']))
    print >> f, "  Reg2Norm       %1.4e \t  %1.4e" % ((gold_flow['rnavg']),
                                                      (reg_flow['rnavg']))
    print >> f, ""
    print >> f, "  TRANSPORT: number of different results: %i" % len(reg_tran['ts'])
    print >> f, "  TRANSPORT:       GOLD        NEW RTOL: %1.e" % tran_rtol
    print >> f, "  Timestep       %.5g  \t     %.5g" % ((gold_tran['tsavg']),
                                                        (reg_tran['tsavg']))
    print >> f, "  Newton         %.5g   \t    %.5g" % ((gold_tran['niavg']),
                                                        (reg_tran['niavg']))
    print >> f, "  Newton/TS      %.5g  \t     %.5g" % (gold_tran['niavg']/gold_tran['tsavg'],
                                                        reg_tran['niavg']/reg_tran['tsavg'])
    print >> f, "  Linear         %.5g  \t     %.5g" % ((gold_tran['siavg']),
                                                        (reg_tran['siavg']))
    print >> f, "  Linear/Newt    %.5g  \t     %.5g" % (gold_tran['siavg']/gold_tran['niavg'],
                                                        reg_tran['siavg']/reg_tran['niavg'])
    print >> f, "  Sol2Norm       %1.4e \t  %1.4e" % ((gold_tran['snavg']),
                                                      (reg_tran['snavg']))
    print >> f, "  Reg2Norm       %1.4e \t  %1.4e" % ((gold_tran['rnavg']),
                                                      (reg_tran['rnavg']))
    print >> f, ""
    print >> f, ""
    print >> f, "  FLOW: all of the regression test results: %i" % len(reg_flow['ts'] +
                                                                       reg_flow_same['ts'])
    print >> f, "  FLOW:            GOLD          NEW RTOL: %1.e" % flow_rtol
    print >> f, "  Timestep       %.5g  \t     %.5g" % ((gold_flow['tsavgall']),
                                                        (reg_flow['tsavgall']))
    print >> f, "  Newton         %.5g  \t     %.5g" % ((gold_flow['niavgall']),
                                                        (reg_flow['niavgall']))
    print >> f, "  Newton/TS      %.5g  \t     %.5g" % (gold_flow['niavgall']/gold_flow['tsavgall'],
                                                        reg_flow['niavgall']/reg_flow['tsavgall'])
    print >> f, "  Linear         %.5g  \t     %.5g" % ((gold_flow['siavgall']),
                                                        (reg_flow['siavgall']))
    print >> f, "  Linear/Newt    %.5g  \t     %.5g" % (gold_flow['siavgall']/gold_flow['niavgall'],
                                                        reg_flow['siavgall']/reg_flow['niavgall'])
    print >> f, "  Sol2Norm       %1.4e \t  %1.4e" % ((gold_flow['snavgall']),
                                                      (reg_flow['snavgall']))
    print >> f, "  Reg2Norm       %1.4e \t  %1.4e" % ((gold_flow['rnavgall']),
                                                      (reg_flow['rnavgall']))
    print >> f, ""
    print >> f, "  TRANSPORT:  all of the regression test results: %i" % len(reg_tran['ts'] +
                                                                             reg_tran_same['ts'])
    print >> f, "  TRANSPORT:       GOLD        NEW RTOL: %1.e" % tran_rtol
    print >> f, "  Timestep       %.5g  \t     %.5g" % ((gold_tran['tsavgall']),
                                                        (reg_tran['tsavgall']))
    print >> f, "  Newton         %.5g   \t    %.5g" % ((gold_tran['niavgall']),
                                                        (reg_tran['niavgall']))
    print >> f, "  Newton/TS      %.5g  \t     %.5g" % (gold_tran['niavgall']/gold_tran['tsavgall'],
                                                        reg_tran['niavgall']/reg_tran['tsavgall'])
    print >> f, "  Linear         %.5g  \t     %.5g" % ((gold_tran['siavgall']),
                                                        (reg_tran['siavgall']))
    print >> f, "  Linear/Newt    %.5g  \t     %.5g" % (gold_tran['siavgall']/gold_tran['niavgall'],
                                                        reg_tran['siavgall']/reg_tran['niavgall'])
    print >> f, "  Sol2Norm       %1.4e \t  %1.4e" % ((gold_tran['snavgall']),
                                                      (reg_tran['snavgall']))
    print >> f, "  Reg2Norm       %1.4e \t  %1.4e" % ((gold_tran['rnavgall']),
                                                      (reg_tran['rnavgall']))
    print >> f, ""
    print >> f, ""
    print >> f, "  FLOW: np regression test results: %i" % len(reg_flow_np['ts'])
    print >> f, "  FLOW:            GOLD          NEW RTOL: %1.e" % flow_rtol
    print >> f, "  Timestep       %.5g  \t     %.5g" % ((gold_flow['tsnp']),
                                                        (reg_flow['tsnp']))
    print >> f, "  Newton         %.5g  \t     %.5g" % ((gold_flow['ninp']),
                                                        (reg_flow['ninp']))
    print >> f, "  Newton/TS      %.5g  \t     %.5g" % (gold_flow['ninp']/gold_flow['tsnp'],
                                                        reg_flow['ninp']/reg_flow['tsnp'])
    print >> f, "  Linear         %.5g  \t     %.5g" % ((gold_flow['sinp']),
                                                        (reg_flow['sinp']))
    print >> f, "  Linear/Newt    %.5g  \t     %.5g" % (gold_flow['sinp']/gold_flow['ninp'],
                                                        reg_flow['sinp']/reg_flow['ninp'])
    print >> f, "  Sol2Norm       %1.4e \t  %1.4e" % ((gold_flow['snnp']),
                                                      (reg_flow['snnp']))
    print >> f, "  Reg2Norm       %1.4e \t  %1.4e" % ((gold_flow['rnnp']),
                                                      (reg_flow['rnnp']))
    print >> f, ""
    print >> f, "  TRANSPORT: np regression test results: %i" % len(reg_tran_np['ts'])
    print >> f, "  TRANSPORT:       GOLD        NEW RTOL: %1.e" % tran_rtol
    print >> f, "  Timestep       %.5g  \t     %.5g" % ((gold_tran['tsnp']),
                                                        (reg_tran['tsnp']))
    print >> f, "  Newton         %.5g   \t    %.5g" % ((gold_tran['ninp']),
                                                        (reg_tran['ninp']))
    print >> f, "  Newton/TS      %.5g  \t     %.5g" % (gold_tran['ninp']/gold_tran['tsnp'],
                                                        reg_tran['ninp']/reg_tran['tsnp'])
    print >> f, "  Linear         %.5g  \t     %.5g" % ((gold_tran['sinp']),
                                                        (reg_tran['sinp']))
    print >> f, "  Linear/Newt    %.5g  \t     %.5g" % (gold_tran['sinp']/gold_tran['ninp'],
                                                        reg_tran['sinp']/reg_tran['ninp'])
    print >> f, "  Sol2Norm       %1.4e \t  %1.4e" % ((gold_tran['snnp']),
                                                      (reg_tran['snnp']))
    print >> f, "  Reg2Norm       %1.4e \t  %1.4e" % ((gold_tran['rnnp']),
                                                      (reg_tran['rnnp']))
    print >> f, ""
    print >> f, "  Total Run Time:  GOLD          NEW RTOL"
    print >> f, "  Timestep         271.984 [s]   %s" % (reg['total_run_time'])
    
